import pandas as pd
import numpy as np

cols_gob = ['SECCION','PAN', 'PRI',
       'PRD', 'PVEM', 'PT', 'MC', 'MORENA', 'PANAL', 'PES', 'RSP', 'FXM',
       'C_PRI_PRD', 'C_PVEM_PT_MORENA_PANAL', 'C_PVEM_PT_MORENA',
       'C_PVEM_PT_PANAL', 'C_PVEM_MORENA_PANAL', 'C_PT_MORENA_PANAL',
       'C_PVEM_PT', 'C_PVEM_MORENA', 'C_PVEM_PANAL', 'C_PT_MORENA',
       'C_PT_PANAL', 'C_MORENA_PANAL', 'NO_REGISTRADOS', 'NULOS','LISTA_NOMINAL']

path_gob = 'data/gobernador/NL_GOB_2021.csv'

df_gob = pd.read_csv(path_gob, header = 4, encoding = 'iso-8859-1',
                     engine = 'python', na_values = ['-'], usecols = cols_gob)

df_gob.iloc[:,1:] = df_gob.iloc[:,1:].apply(pd.to_numeric, errors='coerce')

df_agg = df_gob.groupby('SECCION').sum()

new_cols = []
for i in df_agg.columns:
    col = i.split('_')
    
    if 'C' in col:
        col = col[1:]
    else:
        cols = col
        
    new_cols.append('_'.join(col))
df_agg.columns = new_cols

new_dip = pd.DataFrame(columns = ['PAN', 'MC', 'PES', 'RSP', 'FXM',
       'PRI_PRD', 'PVEM_PT_MORENA_PANAL','NO_REGISTRADOS', 'NULOS', 'LISTA_NOMINAL'], index = df_agg.index)

for i in df_agg.index:
    
    new_dip.loc[i,'PVEM_PT_MORENA_PANAL'] = df_agg.loc[i,['PVEM','PT','MORENA', 'PANAL','PVEM_PT_MORENA_PANAL', 'PVEM_PT_MORENA', 'PVEM_PT_PANAL',
                                                          'PVEM_MORENA_PANAL', 'PT_MORENA_PANAL', 'PVEM_PT',
                                                          'PVEM_MORENA','PVEM_PANAL', 'PT_MORENA', 'PT_PANAL',
                                                          'MORENA_PANAL']].sum()
    
    new_dip.loc[i,'PRI_PRD'] = df_agg.loc[i, 'PRD'] + df_agg.loc[i, 'PRI'] + df_agg.loc[i, 'PRI_PRD']
    new_dip.loc[i,'PAN'] = df_agg.loc[i,'PAN']
    new_dip.loc[i,'MC'] = df_agg.loc[i, 'MC']
    new_dip.loc[i,'PES'] = df_agg.loc[i,'PES']
    new_dip.loc[i,'RSP'] = df_agg.loc[i,'RSP']
    new_dip.loc[i,'FXM'] = df_agg.loc[i, 'FXM']
    new_dip.loc[i,'NO_REGISTRADOS'] = df_agg.loc[i, 'NO_REGISTRADOS']
    new_dip.loc[i,'NULOS'] = df_agg.loc[i, 'NULOS']
    new_dip.loc[i,'LISTA_NOMINAL'] = df_agg.loc[i, 'LISTA_NOMINAL']

df_complete = new_dip.fillna(0)
df_complete['TOTAL'] = df_complete.iloc[:, :-1].apply(lambda x: x.sum(), axis = 1)
df_complete['Ganador'] = df_complete[df_complete.columns[:-4]].idxmax(axis=1)

mask = (df_complete['TOTAL']==0)
df_complete.loc[mask, 'Ganador'] = 'Ninguno'

df_per = df_complete.copy()

for i in df_per.columns[:-3]:
    df_per[i] = round((df_per[i]/df_per['TOTAL'])*100, 2)
    
mask = (df_per['TOTAL']==0)
df_per.loc[mask, 'Ganador'] = 'Ninguno'
df_per['PARTICIPACION'] = round(df_per['TOTAL']/df_per['LISTA_NOMINAL'],4)*100
df_per = df_per.fillna(0)

import datetime
now = datetime.datetime.now()

time = str(now.month).zfill(2) + str(now.day).zfill(2)

# Comment for the pipeline
#if not os.path.isdir('data/gen_data/'+time): 
#    os.makedirs('data/gen_data/'+time)

import os
print(os.getcwd)

per = 'data/artifacts/Gob_PER.csv'
complete = 'data/artifacts/Gob_.csv'

df_per.to_csv(per)
df_complete.to_csv(complete)