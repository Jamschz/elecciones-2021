import pandas as pd
import numpy as np

import os

pd.set_option('display.max_columns', None)

cols_mun = ['ID_MUNICIPIO','PAN', 'PRI', 'PRD', 'PVEM', 'PT', 'MC',
       'MORENA', 'PANAL', 'PES', 'RSP', 'FXM', 'CI_1', 'CI_2', 'C_PRI_PRD',
       'C_PVEM_PT_MORENA_PANAL', 'C_PVEM_PT_MORENA', 'C_PVEM_PT_PANAL',
       'C_PVEM_MORENA_PANAL', 'C_PT_MORENA_PANAL', 'C_PVEM_PT',
       'C_PVEM_MORENA', 'C_PVEM_PANAL', 'C_PT_MORENA', 'C_PT_PANAL',
       'C_MORENA_PANAL', 'NO_REGISTRADOS', 'NULOS', 'TOTAL_VOTOS_ASENTADO',
       'TOTAL_VOTOS_CALCULADO']

path_mun = 'data/ayuntamiento/NL_AYUN_2021.csv'

df_mun = pd.read_csv(path_mun, encoding = 'iso-8859-1', header = 4,
                     engine = 'python', na_values = ['-'], usecols = cols_mun)

df_mun.iloc[:,1:] = df_mun.iloc[:,1:].apply(pd.to_numeric, errors='coerce')


coal_mun_path = 'data/ayuntamiento/ayuntanmientos_id.csv'

coal_mun = pd.read_csv(coal_mun_path, usecols = ['ID_AYUNTAMIENTONL', 'JHHNL', 'VFNL'])
coal_mun['ID_AYUNTAMIENTONL'] = coal_mun['ID_AYUNTAMIENTONL'].astype(str)
coal_mun['ID_AYUNTAMIENTONL'] = pd.to_numeric(coal_mun['ID_AYUNTAMIENTONL'])

group = df_mun.groupby('ID_MUNICIPIO').sum().reset_index()
df_agg = pd.merge(group, coal_mun, left_on = 'ID_MUNICIPIO', right_on = 'ID_AYUNTAMIENTONL').set_index('ID_AYUNTAMIENTONL').drop('ID_MUNICIPIO', axis = 1)

new_cols = []

for i in df_agg.columns:
    col = i.split('_')

    if 'C' in col:
        col = col[1:]
    else:
        cols = col

    new_cols.append('_'.join(col))
    
df_agg.columns = new_cols

new_dip = pd.DataFrame(columns = ['PAN', 'PRI', 'PRD', 'PVEM', 'PT', 'MC', 'MORENA', 'PANAL', 'PES',
       'RSP', 'FXM', 'CI_1', 'CI_2', 'PRI_PRD', 'PVEM_PT_MORENA_PANAL',
       'PVEM_PT_MORENA', 'PVEM_PT_PANAL', 'PVEM_MORENA_PANAL',
       'PT_MORENA_PANAL', 'PVEM_PT', 'PVEM_MORENA', 'PVEM_PANAL', 'PT_MORENA',
       'PT_PANAL', 'MORENA_PANAL'], index = df_agg.index)

for i in df_agg.index:
    
    if df_agg.loc[i,'JHHNL']==1:
        new_dip.loc[i,'PVEM_PT_MORENA_PANAL'] = df_agg.loc[i,['PVEM', 'PT', 'MORENA', 'PANAL','PVEM_PT_MORENA_PANAL',
                                                        'PVEM_PT_MORENA', 'PVEM_PT_PANAL', 'PVEM_MORENA_PANAL',
                                                        'PT_MORENA_PANAL', 'PVEM_PT', 'PVEM_MORENA', 'PVEM_PANAL',
                                                        'PT_MORENA','PT_PANAL', 'MORENA_PANAL']].sum()
        
    elif df_agg.loc[i, 'JHHNL']==0:
        new_dip.loc[i,'PT'] = df_agg.loc[i, 'PT']
        new_dip.loc[i,'PVEM'] = df_agg.loc[i,'PVEM']
        new_dip.loc[i,'MORENA'] = df_agg.loc[i,'MORENA']
        new_dip.loc[i,'PANAL'] = df_agg.loc[i,'PANAL']
        
    if df_agg.loc[i,'VFNL']==1:
        new_dip.loc[i,'PRI_PRD'] = df_agg.loc[i,['PRI', 'PRD','PRI_PRD']].sum()
        
    elif df_agg.loc[i,'VFNL']==0:
        new_dip.loc[i,'PRI'] = df_agg.loc[i,'PRI']
        new_dip.loc[i,'PRD'] = df_agg.loc[i,'PRD']
        
    
    new_dip.loc[i,'PAN'] = df_agg.loc[i, 'PAN']
    new_dip.loc[i,'MC'] = df_agg.loc[i, 'MC']
    new_dip.loc[i,'PES'] = df_agg.loc[i,'PES']
    new_dip.loc[i,'RSP'] = df_agg.loc[i,'RSP']
    new_dip.loc[i,'FXM'] = df_agg.loc[i, 'FXM']
    new_dip.loc[i,'CI_1'] = df_agg.loc[i,'CI_1']
    new_dip.loc[i,'CI_2'] = df_agg.loc[i,'CI_2']
    new_dip.loc[i,'JHHNL'] = df_agg.loc[i,'JHHNL']
    new_dip.loc[i,'VFNL'] = df_agg.loc[i,'VFNL']

df_complete = new_dip.fillna(0)
df_complete['TOTAL'] = df_complete.iloc[:, :-2].apply(lambda x: x.sum(), axis = 1)
df_complete['Ganador'] = df_complete[df_complete.columns[:-3]].idxmax(axis=1)

mask = (df_complete['TOTAL']==0)
df_complete.loc[mask, 'Ganador'] = 'Ninguno'

df_per = df_complete.copy()

for i in df_per.columns[:-3]:
    df_per[i] = round((df_per[i]/df_per['TOTAL'])*100, 2)
    
mask = (df_per['TOTAL']==0)
df_per.loc[mask, 'Ganador'] = 'Ninguno'    
df_per = df_per.fillna(0)

import datetime
now = datetime.datetime.now()

time = str(now.month).zfill(2) + str(now.day).zfill(2)

# Comment for the pipeline
#if not os.path.isdir('data/gen_data/'+time):
#   os.makedirs('data/gen_data/'+time)

import os
print(os.getcwd)

per = 'data/artifacts/Mun_PREP_PER.csv'
complete = 'data/artifacts/Mun_PREP.csv'

df_per.to_csv(per)
df_complete.to_csv(complete)